﻿using System.Data.Entity;
using System.Data.EntityClient;
using System.Data.SqlClient;

namespace SpearOne.RepositoryPattern.EntityFramework5.Extensions
{
    public static class DbContextExtensions
    {
        public static void ChangeDatabaseConnection(this DbContext context, string initialCatalog = null, string dataSource = null, string userId = null,
                                          string password = null, bool? integratedSecuity = null, string configConnectionStringName = null)
        {
            var efConfigName = string.IsNullOrEmpty(configConnectionStringName)
                    ? context.GetType().Name
                    : configConnectionStringName;

            var entityConnectionStringBuilder = new EntityConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings[efConfigName].ConnectionString);

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder(entityConnectionStringBuilder.ProviderConnectionString);

            if (!string.IsNullOrEmpty(initialCatalog))
                sqlConnectionStringBuilder.InitialCatalog = initialCatalog;

            if (!string.IsNullOrEmpty(dataSource))
                sqlConnectionStringBuilder.DataSource = dataSource;

            if (!string.IsNullOrEmpty(userId))
                sqlConnectionStringBuilder.UserID = userId;

            if (!string.IsNullOrEmpty(password))
                sqlConnectionStringBuilder.Password = password;

            if (integratedSecuity.HasValue)
                sqlConnectionStringBuilder.IntegratedSecurity = integratedSecuity.Value;

            context.Database.Connection.ConnectionString = sqlConnectionStringBuilder.ConnectionString;
        }
    }
}
