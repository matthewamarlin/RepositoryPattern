﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SpearOne.RepositoryPattern.Interfaces;

namespace SpearOne.RepositoryPattern.EntityFramework5.Generic
{
    /// <summary>
    ///     Represents a Fluent Query class
    /// </summary>
    /// <typeparam name="TContext">
    ///         The Entitfy Framework DbContext to use.
    /// </typeparam>
    /// <typeparam name="TEntity">
    ///         The database entity which this query is intended for.
    /// </typeparam>
    public sealed class Query<TContext, TEntity> : IQuery<TEntity>
        where TContext : DbContext, new()
        where TEntity : class
    {
        private readonly Expression<Func<TEntity, bool>> _expression;
        private readonly List<Expression<Func<TEntity, object>>> _includes;
        private Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> _orderBy;

        private readonly Repository<TContext, TEntity> _repository;


        /// <summary>
        ///     Starts a new query on the given repository.
        /// </summary>
        /// <param name="repository">The repository to query</param>
        public Query(Repository<TContext, TEntity> repository)
        {
            _repository = repository;
            _includes = new List<Expression<Func<TEntity, object>>>();
        }

        public Query(Repository<TContext, TEntity> repository, IQueryObject<TEntity> queryObject)
            : this(repository)
        {
            _expression = queryObject.Evaluate();
        }

        public Query(Repository<TContext, TEntity> repository, Expression<Func<TEntity, bool>> expression)
            : this(repository)
        {
            _expression = expression;
        }

        /// <summary>
        ///  Applies an order to the entities.
        /// </summary>
        /// <param name="orderBy">An expression stipulating how to order the entities</param>
        /// <returns>An ordered IQueryable of type <typeparam name="TEntity"></typeparam> </returns>
        public IQuery<TEntity> ApplyOrder(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy)
        {
            _orderBy = orderBy;
            return this;
        }

        /// <summary>
        ///  Includes the specified navigation property for the current query. For example, say an Orders table has a foreign key to an OrderDetails table.
        ///  By specifying  "x => x.OrderDetails" will result in all the order details for the current Orders query to be included in the query results.
        /// </summary>
        /// <param name="expression">An expression naming the navigaional property to include in the query.</param>
        /// <returns>An IQueryable of type <typeparamref name="TEntity"/> incuding the specified navigational property entites.</returns>
        public IQuery<TEntity> Include(Expression<Func<TEntity, object>> expression)
        {
            _includes.Add(expression);
            return this;
        }

        /// <summary>
        ///     Selects a group of entites that form part of a particular page, 
        ///     given the page number and desired page size.
        /// </summary>
        /// <param name="page">
        ///     The page to return
        /// </param>
        /// <param name="pageSize">
        ///     The amount of entities to include on a single page
        /// </param>
        /// <param name="totalCount">
        ///     The amount of entities which form part of the query thus far, before appliying this method.
        /// </param>
        /// <returns>
        ///     Returns the entitie swhich form on the selected page as an IEnumerable of type <typeparamref name="TEntity"/> 
        /// </returns>
        public IEnumerable<TEntity> SelectPage(int page, int pageSize, out int totalCount)
        {
            if (_orderBy == null)
                throw new NotSupportedException("Operation Not Supported: Cannot select a page when the etities have no order defined." +
                                                "Please call the ApplyOrder method on the Query berfore calling the SelectPage method.");

            totalCount = _repository.Select(_expression).Count();
            return _repository.Select(_expression, _orderBy, _includes, page, pageSize);
        }

        /// <summary>
        ///     Selects the given entities using the constructed query and returns them as an IEnumerable of type <typeparamref name="TEntity"/>
        /// </summary>
        /// <returns>
        ///     Returns an IEnumerable of type <typeparamref name="TEntity"/>
        /// </returns>
        public IEnumerable<TEntity> Select()
        {
            return _repository.Select(_expression, _orderBy, _includes);
        }

        /// <summary>
        ///     Selects the entities which form part of the current query expression and applies a Projection to it. 
        ///     That is each element is projected to a new type using the supplied projection Expression.
        /// </summary>
        /// <typeparam name="TResult">
        ///     The projected Type of the entities being selected
        /// </typeparam>
        /// <param name="projection">
        ///     The projection expression to use.
        /// </param>
        /// <returns>
        ///     Returns an IEnumerable of type <typeparamref name="TResult"/>
        /// </returns>
        public IEnumerable<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> projection)
        {
            return _repository.Select(_expression, _orderBy, _includes).Select(projection);
        }
    }
}
