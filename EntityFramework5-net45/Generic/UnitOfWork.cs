﻿using System;
using System.Data.Entity;
using SpearOne.RepositoryPattern.Interfaces;

namespace SpearOne.RepositoryPattern.EntityFramework5.Generic
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext>
        where TContext : DbContext, new()
    {
        /// <summary>
        /// The DbContext of type <typeparamref name="TContext"/> that is associated with this UnitOfWork.
        /// </summary>
        public TContext Context { get; private set; }

        /// <summary>
        ///     Creates the UnitOfWork for the Given DbContext. If no DbContext is provided a 
        ///     new instance is created based on the generic type paramaters.
        /// </summary>
        /// <param name="dbContext"></param>
        public UnitOfWork(TContext dbContext = null)
        {
            Context = dbContext ?? new TContext();
        }

        #region Unit Of Work Methods

        /// <summary>
        /// Commits the current changes to the database.
        /// </summary>
        public void Commit()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// IDisposable Implementation. Disposes the database context if it hasnt already been disposed, 
        /// effectively reverting any uncommitted changes to the context.
        /// </summary>
        public void Dispose(bool disposing)
        {
            if (!disposing || Context == null)
                return;

            Context.Dispose();
            Context = null;
        }

        public void Dispose()
        {
            Dispose(true);

            /* 
             * Request the Garbage collector, not to finalize the context as it has already been cleaned up fully.
             * This is an optimization for the CLR.
             */
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
