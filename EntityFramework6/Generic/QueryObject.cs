﻿using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using LinqKit; //http://www.albahari.com/nutshell/linqkit.aspx
using SpearOne.RepositoryPattern.Interfaces;

namespace SpearOne.RepositoryPattern.EntityFramework6.Generic
{
    /// <summary>
    ///     Represents a Database Query Expression for the type <typeparamref name="TEntity"/>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class QueryObject<TEntity> : IQueryObject<TEntity>
    {
        private Expression<Func<TEntity, bool>> _query;
        
        /// <summary>
        ///     Creates a QueryObject of type <typeparamref name="TEntity"/> that can be used to query a <typeparamref name="TEntity"/> 
        ///     repository using the specified experession. If no expression is specified a default expression will be created that will 
        ///     always return true and thus return all the results
        /// </summary>
        /// <param name="query"></param>
        public QueryObject(Expression<Func<TEntity, bool>> query = null)
        {
            _query = query;
        }

        /// <returns>
        ///     Returns the current QueryObject of type <typeparamref name="TEntity"/> as an Expression.
        ///     If the query ObjectObject conatins no expression a default epression will be created that 
        ///     return true for every element, effectively selecting all the elements.
        /// </returns>
        public virtual Expression<Func<TEntity, bool>> Evaluate()
        {
            return _query;
        }

        /// <summary>
        ///     Combinds the given Expression with the current query expression in a Logical AND manner.
        /// </summary>
        /// <param name="query">
        ///     The Expression to AND with the current query expression.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     In the expression A and B and C and D, if any of the conditions (A, B, C, or D) are False, 
        ///     the entire expression is False. So if you start an "and" expression with False, no matter 
        ///     what else you AND to it, the entire expression will be False.
        /// </remarks>
        public virtual IQueryObject<TEntity> And(Expression<Func<TEntity, bool>> query)
        {
            _query = (_query == null) ?  query : _query.And(query.Expand());
            
            return this;
        }

        /// <summary>
        ///     Combinds the given Expression with the current query expression in a Logical OR manner.
        /// </summary>
        /// <param name="query">
        ///     The Expression to OR with the current query expression.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     In the expression A or B or C or D, if any of the conditions (A, B, C, or D) are True, 
        ///     the entire expression is True. So if you start an "or" expression with True, no matter 
        ///     what else you OR to it, the entire expression will be True.
        /// </remarks>
        public virtual IQueryObject<TEntity> Or(Expression<Func<TEntity, bool>> query)
        {
            _query = (_query == null) ? query : _query.Or(query.Expand());
            return this;
        }

        /// <summary>
        ///     Combinds the expressions of the given QueryObject and the current QueryObject in a 
        ///     Logical AND manner.
        /// </summary>
        /// <param name="queryObject">
        ///     The QueryObject to combine.
        /// </param>
        /// <returns>
        ///     An Expression representing the Logical AND of the two QueryObjects.
        /// </returns>
        /// <remarks>
        ///     In the expression A and B and C and D, if any of the conditions (A, B, C, or D) are False, 
        ///     the entire expression is False. So if you start an "and" expression with False, no matter 
        ///     what else you AND to it, the entire expression will be False.
        /// </remarks>
        public virtual IQueryObject<TEntity> And(IQueryObject<TEntity> queryObject)
        {
            return And(queryObject.Evaluate());
        }

        /// <summary>
        ///     Combinds the expressions of the given QueryObject and the current QueryObject in a 
        ///     Logical OR manner.
        /// </summary>
        /// <param name="queryObject">
        ///     The QueryObject to OR with the current QueryObject.
        /// </param>
        /// <returns>
        ///     An Expression representing the Logical OR of the two QueryObjects.
        /// </returns>
        /// <remarks>
        ///     In the expression A or B or C or D, if any of the conditions (A, B, C, or D) are True, 
        ///     the entire expression is True. So if you start an "or" expression with True, no matter 
        ///     what else you OR to it, the entire expression will be True.
        /// </remarks>
        public virtual IQueryObject<TEntity> Or(IQueryObject<TEntity> queryObject)
        {
            return Or(queryObject.Evaluate());
        }
    }
}
