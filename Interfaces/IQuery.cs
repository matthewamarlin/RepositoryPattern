﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SpearOne.RepositoryPattern.Interfaces
{
    public interface IQuery<TEntity> where TEntity : class
    {
        
        IQuery<TEntity> ApplyOrder(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy);


        IQuery<TEntity> Include(Expression<Func<TEntity, object>> expression);

        /// <summary>
        ///     Selects a group of entites that form part of a particular page, 
        ///     given the page number and desired page size.
        /// </summary>
        /// <param name="page">
        ///     The page to return
        /// </param>
        /// <param name="pageSize">
        ///     The amount of entities to include on a single page
        /// </param>
        /// <param name="totalCount">
        ///     The amount of entities which form part of the query thus far, before appliying this method.
        /// </param>
        /// <returns>
        ///     Returns the entitie swhich form on the selected page as an IEnumerable of type <typeparamref name="TEntity"/> 
        /// </returns>
        IEnumerable<TEntity> SelectPage(int page, int pageSize, out int totalCount);

        /// <summary>
        ///     Selects the entities which form part of the current query expression and applies a Projection to it. 
        ///     That is each element is projected to a new type using the supplied projection Expression.
        /// </summary>
        /// <typeparam name="TResult">
        ///     The projected Type of the entities being selected
        /// </typeparam>
        /// <param name="projection">
        ///     The projection expression to use.
        /// </param>
        /// <returns>
        ///     Returns an IEnumerable of type <typeparamref name="TResult"/>
        /// </returns>
        IEnumerable<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> projection = null);

        /// <summary>
        ///     Selects the given entities using the constructed query and returns them as an IEnumerable of type <typeparamref name="TEntity"/>
        /// </summary>
        /// <returns>
        ///     Returns an IEnumerable of type <typeparamref name="TEntity"/>
        /// </returns>
        IEnumerable<TEntity> Select();
    }
}