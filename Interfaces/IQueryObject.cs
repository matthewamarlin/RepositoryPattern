﻿using System;
using System.Linq.Expressions;

namespace SpearOne.RepositoryPattern.Interfaces
{
    public interface IQueryObject<TEntity>
    {
        
        /// <returns>
        ///     Returns the current QueryObject of type <typeparamref name="TEntity"/> as an Expression.
        /// </returns>
        Expression<Func<TEntity, bool>> Evaluate();

        /// <summary>
        ///     Combinds the given Expression with the current query expression in a Logical AND manner.
        /// </summary>
        /// <param name="query">
        ///     The Expression to AND with the current query expression.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     In the expression A and B and C and D, if any of the conditions (A, B, C, or D) are False, 
        ///     the entire expression is False. So if you start an "and" expression with False, no matter 
        ///     what else you AND to it, the entire expression will be False.
        /// </remarks>
        IQueryObject<TEntity> And(Expression<Func<TEntity, bool>> query);


        /// <summary>
        ///     Combinds the given Expression with the current query expression in a Logical OR manner.
        /// </summary>
        /// <param name="query">
        ///     The Expression to OR with the current query expression.
        /// </param>
        /// <returns></returns>
        /// <remarks>
        ///     In the expression A or B or C or D, if any of the conditions (A, B, C, or D) are True, 
        ///     the entire expression is True. So if you start an "or" expression with True, no matter 
        ///     what else you OR to it, the entire expression will be True.
        /// </remarks>
        IQueryObject<TEntity> Or(Expression<Func<TEntity, bool>> query);

        /// <summary>
        ///     Combinds the expressions of the given QueryObject and the current QueryObject in a 
        ///     Logical AND manner.
        /// </summary>
        /// <param name="queryObject">
        ///     The QueryObject to combine.
        /// </param>
        /// <returns>
        ///     An Expression representing the Logical AND of the two QueryObjects.
        /// </returns>
        /// <remarks>
        ///     In the expression A and B and C and D, if any of the conditions (A, B, C, or D) are False, 
        ///     the entire expression is False. So if you start an "and" expression with False, no matter 
        ///     what else you AND to it, the entire expression will be False.
        /// </remarks>
        IQueryObject<TEntity> And(IQueryObject<TEntity> queryObject);

        /// <summary>
        ///     Combinds the expressions of the given QueryObject and the current QueryObject in a 
        ///     Logical AND manner.
        /// </summary>
        /// <param name="queryObject">
        ///     The QueryObject to OR with the current QueryObject.
        /// </param>
        /// <returns>
        ///     An Expression representing the Logical OR of the two QueryObjects.
        /// </returns>
        /// <remarks>
        ///     In the expression A or B or C or D, if any of the conditions (A, B, C, or D) are True, 
        ///     the entire expression is True. So if you start an "or" expression with True, no matter 
        ///     what else you OR to it, the entire expression will be True.
        /// </remarks>
        IQueryObject<TEntity> Or(IQueryObject<TEntity> queryObject);
    }
}
