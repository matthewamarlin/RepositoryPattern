﻿using System;

namespace SpearOne.RepositoryPattern.Interfaces
{
    public interface IUnitOfWork<out TContext> : IDisposable
    {
        void Commit();

        TContext Context { get; }

        void Dispose(bool disposing);
    }
}
