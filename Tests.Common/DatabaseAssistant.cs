﻿using Microsoft.SqlServer.Dac;
using System;
using System.Data.SqlClient;

namespace SpearOne.RepositoryPattern.Tests.Common
{
    public class DatabaseAssistant
    {
        private static readonly string DbConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Local"].ConnectionString;
        private static readonly string DbName = "TestDatabase";
        private static readonly string BacPacPath = string.Concat(AppDomain.CurrentDomain.BaseDirectory, @"\..\..\..\Tests.Common\Database\TestDatabase.bacpac");

        public static void Setup()
        {
            try
            {
                var dbServices = new DacServices(DbConnectionString);
                var bacPac = BacPackage.Load(BacPacPath);
                dbServices.ImportBacpac(bacPac, DbName);
            }
            catch (DacServicesException exception)
            {
                if (exception.Message.Contains(
                        "Data cannot be imported into target because it contains one or more user objects. " +
                        "Import should be performed against a new, empty database"))
                {
                    TearDown();
                    Setup();
                }
                else
                {
                    throw;
                }
            }
        }

        public static void TearDown()
        {
            using (var connection = new SqlConnection(DbConnectionString))
            {
                connection.Open();

                // If you used master db as Initial Catalog, 
                // there is no need to change database
                connection.ChangeDatabase("master");

                string rollbackCommand = @"ALTER DATABASE [" + DbName + "] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE";

                var sqlRollbackCommand = new SqlCommand(rollbackCommand, connection);

                sqlRollbackCommand.ExecuteNonQuery();


                string deleteCommand = @"DROP DATABASE [" + DbName + "]";

                var sqlDeleteCommand = new SqlCommand(deleteCommand, connection);

                sqlDeleteCommand.ExecuteNonQuery();
            }
        }
    }
}
