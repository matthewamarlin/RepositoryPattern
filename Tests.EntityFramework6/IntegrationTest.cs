﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpearOne.RepositoryPattern.EntityFramework6.Generic;
using SpearOne.RepositoryPattern.Tests.EntityFramework6.Generated;

namespace SpearOne.RepositoryPattern.Tests.EntityFramework6
{
    /// <summary>
    ///     We can only perform Integration Tests as unit Testing with a FakeDbContext/InMemoryObjectSet,
    ///     we are no longer using Linq-to-SQL instead we would then be using Linq-to-Objects which renders 
    ///     the tests pointless:
    ///         http://stackoverflow.com/questions/6904139/fake-dbcontext-of-entity-framework-4-1-to-test
    /// </summary>
    [TestClass]
    public class RepositoryPatternTests
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //// Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void Initialize(TestContext context)
        //{ }

        //// Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void Cleanup()
        //{ }

        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion

        [TestMethod]
        public void Insert_CanInsert_ButNotInsertedInDatabaseBeforeUnitOfWorkIsCommitted()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                const byte id = 23;

                var awBuildVersion = new AWBuildVersion
                {
                    Database_Version = "Test Version",
                    ModifiedDate = DateTime.Now,
                    SystemInformationID = id,
                    VersionDate = DateTime.Now
                };

                var repo = new Repository<AdventureWorks2012Entities, AWBuildVersion>(unitOfWork);
                repo.Insert(awBuildVersion);

                var shouldbeNull = repo.Get(x => x.SystemInformationID == id).FirstOrDefault();

                Assert.AreEqual(null, shouldbeNull);

                unitOfWork.Commit();

                var dbAppLog = repo.Get(x => x.SystemInformationID == id).FirstOrDefault();

                Assert.AreEqual(awBuildVersion, dbAppLog);
            }
        }

        [TestMethod]
        public void QueryAndSelect_ReturnsAllObjectInTable_SameAsGetAll()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var transactionInfoRepo = new Repository<AdventureWorks2012Entities, Person>(unitOfWork);

                var result = transactionInfoRepo.GetAll();
                var expected = transactionInfoRepo.Query().Select();

                Assert.IsNotNull(result);
                Assert.IsNotNull(expected);
                Assert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void QueryObject_OrFucntion_ReturnsEntitesWhichMatchCombineQueries()
        {
            Expression<Func<BusinessEntityAddress, bool>> query1 = (x => x.rowguid == Guid.Parse("3C915B31-7C05-4A05-9859-0DF663677240"));
            Expression<Func<BusinessEntityAddress, bool>> query2 = (x => x.BusinessEntityID == 15);
            Expression<Func<BusinessEntityAddress, bool>> query3 = (x => x.AddressTypeID == 2);

            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var tranImportRequestQueueRepo = new Repository<AdventureWorks2012Entities, BusinessEntityAddress>(unitOfWork);

                var queryObject = new QueryObject<BusinessEntityAddress>(query1).Or(query2).Or(query3);

                var combinedResults = tranImportRequestQueueRepo.Query(queryObject).Select().ToList();

                var query1Result = tranImportRequestQueueRepo.Get(query1).FirstOrDefault();
                var query2Result = tranImportRequestQueueRepo.Get(query2).FirstOrDefault();
                var query3Result = tranImportRequestQueueRepo.Get(query3).FirstOrDefault();

                Assert.IsTrue(combinedResults.Contains(query1Result));
                Assert.IsTrue(combinedResults.Contains(query2Result));
                Assert.IsTrue(combinedResults.Contains(query3Result));
                Assert.AreEqual(3, combinedResults.Count());
            }
        }

        [TestMethod]
        public void QueryObject_AndFunction_ReturnsEntriesWhichMatchTheCombineQuery()
        {
            //Arrange
            DateTime searchDate;
            DateTime.TryParse("2007-12-15 00:00:00.000", out searchDate);

            Expression<Func<ProductReview, bool>> query1 = (x => x.ReviewerName == "John Smith");
            Expression<Func<ProductReview, bool>> query2 = (x => DbFunctions.DiffSeconds(x.ReviewDate, searchDate) < 1);

            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, ProductReview>(unitOfWork);

                //Act
                var expected1 = repo.Get(x => x.ProductID == 1).First();
                var expected2 = repo.Get(x => x.ProductID == 2).First();

                var queryObject = new QueryObject<ProductReview>(query1).And(query2);
                var result = repo.Query(queryObject).Select().ToList();

                //Assert
                Assert.IsTrue(result.Contains(expected1));
                Assert.IsTrue(result.Contains(expected2));
                Assert.AreEqual(1, result[0].ProductID);
                Assert.AreEqual(2, result[1].ProductID);
            }
        }

        [TestMethod]
        public void QueryObject_AndFunctionWithOrFunction()
        {
            var rowid = Guid.Parse("A3F2FA3A-22E1-43D8-A131-A9B89C32D8EA"); //341
            Expression<Func<Product, bool>> query1 = (x => x.rowguid == rowid);
            Expression<Func<Product, bool>> query2 = (x => x.ProductNumber == "AR - 5381");
            Expression<Func<Product, bool>> query3 = (x => x.SafetyStockLevel > 800);

            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                var queryObject = new QueryObject<Product>(query1).And(query2).Or(query3);

                var results = repo.Query(queryObject).Select().ToList();

                var expected1 = repo.Get(x => x.ProductID == 341).FirstOrDefault();
                var expected2 = repo.Get(x => x.ProductID == 1).FirstOrDefault();
                var expected3 = repo.Get(query3).ToList();

                Assert.IsTrue(results.Contains(expected1));
                Assert.IsTrue(results.Contains(expected2));
                Assert.IsTrue(expected3.All(results.Contains));
            }
        }


        [TestMethod]
        public void Insert_CanInsertAnAssociation_CanFindsEntriesByKey()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                try
                {
                    var productModelId = 125444;
                    var productModel = new ProductModel()
                    {
                        CatalogDescription = "Description",
                        Instructions = "Instructions",
                        ModifiedDate = DateTime.Now,
                        Name = "Name",
                        ProductModelID = productModelId,
                        //ProductModelIllustrations = null,
                    };


                    var productModelIllustration = new ProductModelIllustration
                    {
                        Illustration = new Illustration
                        {
                            Diagram = "DiagramText",
                            //IllustrationID = null,
                            ModifiedDate = DateTime.Now,
                            //ProductModelIllustrations = null,
                        },
                        //IllustrationID = 
                        ModifiedDate = DateTime.Now,
                        ProductModel = productModel
                    };

                    var repo = new Repository<AdventureWorks2012Entities, ProductModelIllustration>(unitOfWork);

                    repo.Insert(productModelIllustration);
                    unitOfWork.Commit();

                    var result = repo.Get(item => item.ProductModelID == productModelId).First();

                    Assert.AreEqual(productModelIllustration, result);
                }
                catch (Exception exception)
                {
                    //For dubug
                    var validationErrors = unitOfWork.Context.GetValidationErrors();
                    throw exception;
                }
            }
        }

        [TestMethod]
        public void ApplyOrderAndSelectPage_CanSelectPage_ReturnsCorrectElements()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var tranImportFailureRepo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                int total;

                var secondPage = tranImportFailureRepo.Query()
                                          .ApplyOrder(x => x.OrderByDescending(y => y.ProductNumber))
                                          .SelectPage(2, 5, out total);

                var pageList = secondPage.ToList();
                var pageCount = pageList.Count;

                Assert.AreEqual(5, pageCount);
                Assert.AreEqual("TT-R982", pageList[0].ProductNumber);
                Assert.AreEqual("TT-M928", pageList[1].ProductNumber);
                Assert.AreEqual("TP-0923", pageList[2].ProductNumber);
                Assert.AreEqual("TO-2301", pageList[3].ProductNumber);
                Assert.AreEqual("TO-2301", pageList[4].ProductNumber);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void SelectPage_WithoutCallingApplyOrderFunctionFirst_ThrowsException()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var tranImportRequestQueueRepo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                int total;
                tranImportRequestQueueRepo.Query().SelectPage(1, 5, out total); //Should Fail
            }
        }

        [TestMethod]
        public void Update_CanUpdateAnEntity_CanFindTheUpdatedEntity()
        {
            const int pk = 10;
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                try
                {
                    var repo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                    var expected = repo.Find(pk);
                    const string newProductNumber = "NN2324";
                    expected.ProductNumber = newProductNumber;

                    repo.Update(expected);
                    unitOfWork.Commit();

                    var result = repo.Find(pk);

                    Assert.AreEqual(321, result.ProductID);
                    Assert.AreEqual(expected, result);
                    Assert.AreEqual(newProductNumber, result.ProductNumber);
                }
                catch (Exception exception)
                {
                    var validationErrors = unitOfWork.Context.GetValidationErrors();
                    throw;
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Update_FailsToUpdateWithNewEntityReference_ThrowsException()
        {
            const int id = 10;
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, AWBuildVersion>(unitOfWork);

                var old = repo.Find(id);

                var expected = new AWBuildVersion()
                {
                    ModifiedDate = DateTime.Now,
                    Database_Version = old.Database_Version,
                    SystemInformationID = old.SystemInformationID,
                    VersionDate = old.VersionDate
                };

                repo.Update(expected);
                unitOfWork.Commit();

                // The test should have already faild at this point.
            }
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public void Delete_DeleteFailsDueToForeignKeyConstraint_ThrowsException()
        {

            //Attempt to delete a BusinessEntity which has a linked Store (Thus it would create an orphan)
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, Store>(unitOfWork);

                repo.Delete(13);
                unitOfWork.Commit();

                //The test should fail have already failed at this point.
            }
        }


        [TestMethod]
        public void Delete_CanDeleteEntityInOneToManyRelationship_ReturnsNullOnFind()
        {
            //Arrange
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var personRepo = new Repository<AdventureWorks2012Entities, Person>(unitOfWork);
                var emailRepo = new Repository<AdventureWorks2012Entities, EmailAddress>(unitOfWork);


                //Act
                var person = personRepo.Find(4);

                var email = emailRepo.Find(person.EmailAddresses.First().EmailAddressID);

                person.EmailAddresses.Remove(email);

                emailRepo.Delete(email);

                unitOfWork.Commit();

                //Assert
                var personResult = personRepo.Find(4);
                var emailResult = emailRepo.Get(x => x.BusinessEntityID == personResult.BusinessEntityID).First();

                Assert.IsNull(emailResult);
                Assert.IsTrue(personResult.EmailAddresses.Count == 0);
            }
        }

        [TestMethod]
        public void InsertRange_CanInsertMultipleEntities_CanFindInsertedRange()
        {
            //Arrange
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, AWBuildVersion>(unitOfWork);


                var list = new List<AWBuildVersion>
                {
                    new AWBuildVersion
                    {
                        Database_Version = "123",
                        ModifiedDate = DateTime.Now,
                        //SystemInformationID = 
                        VersionDate = DateTime.Now,
                    },
                    new AWBuildVersion
                    {
                        Database_Version = "456",
                        ModifiedDate = DateTime.Now,
                        //SystemInformationID = 
                        VersionDate = DateTime.Now,
                    },
                    new AWBuildVersion
                    {
                        Database_Version = "789",
                        ModifiedDate = DateTime.Now,
                        //SystemInformationID = 
                        VersionDate = DateTime.Now,
                    },

                };

                //Act

                repo.InsertRange(list);

                unitOfWork.Commit();

                //Assert

                var all = repo.GetAll().ToList();

                foreach (var expected in list)
                {
                    Assert.IsTrue(all.Contains(expected));
                }
            }
        }

        [TestMethod]
        public void LazyGet_ReturnsTheSameAsGet_AreEqual()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var repo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                // This line will only query the database and return the resulting set.                                                               
                var result = repo.LazyGet(x => x.DaysToManufacture == 2).First(x => x.ProductID == 748);

                //This query will query the database for the first expression, then the second level of filering will occur in memory.
                var expected = repo.Get(x => x.DaysToManufacture == 2).First(x => x.ProductID == 748);

                Assert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void LazyGetAll_ReturnsTheSameAsGetAll_AreEqual()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var transactionInfoRepo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);

                var result = transactionInfoRepo.LazyGetAll().ToList();

                var expected = transactionInfoRepo.GetAll().ToList();

                Assert.AreEqual(expected, result);
            }
        }

        [TestMethod]
        public void Include_IncludesATablesEntries()
        {
            using (var unitOfWork = new UnitOfWork<AdventureWorks2012Entities>())
            {
                var transactionImportRequestQueueRepo = new Repository<AdventureWorks2012Entities, Product>(unitOfWork);
                var result = transactionImportRequestQueueRepo.Query(x => x.ProductID == 317)
                    .Include(x => x.ProductDocument)
                    .Select()
                    .First();


                Assert.IsNotNull(result);
                Assert.AreEqual("LL Crankarm", result.Name);
                Assert.AreEqual(51, result.ProductDocument.ModifiedDate == DateTime.Parse("2008-01-30 13:51:58.103"));
            }
        }


    }
}
